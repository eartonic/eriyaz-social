# eriyaz-social

## Get code in your local directory ##
* Open web page: https://source.cloud.google.com/eriyaz-social/eriyaz-social
* Click on 'Clone' (top right of the page)
* Follow the instructions given in the opened popup.
## Enable Google Authentication ##
* Open web page: https://console.firebase.google.com/u/0/project/eriyaz-social-dev/settings/general/android:com.eriyaz.social.debug
* Add ssh key of your local machine, under Add FingerPrint